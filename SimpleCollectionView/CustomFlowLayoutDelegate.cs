using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace SimpleCollectionView
{
	public class CustomFlowLayoutDelegate: UICollectionViewDelegateFlowLayout
	{
		public CustomFlowLayoutDelegate ()
		{
			// 
//			HeaderReferenceSize = new SizeF (100, 100);
//			SectionInset = new UIEdgeInsets (20, 20, 20, 20);
//			ScrollDirection = UICollectionViewScrollDirection.Vertical;
		}

		public override SizeF GetSizeForItem (UICollectionView collectionView, UICollectionViewLayout layout, 
		                                      NSIndexPath indexPath)
		{
			if (indexPath.Row % 2 == 0) {
				return new SizeF(100, 100);
			} else {
				return new SizeF(50, 50);
			}
		}
	}
}

